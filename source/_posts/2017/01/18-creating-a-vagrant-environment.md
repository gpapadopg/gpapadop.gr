---
title: Creating a Vagrant environment from scratch
tags:
    - Vagrant
---

In order to start, create a vagrant environment based on Ubuntu Trusty (of
course you can start with something of your choice):

    # create the machine
    vagrant init ubuntu/trusty
    
    # boot it
    vagrant up
    
    # get into
    vagrant ssh
    
    # configure it
    sudo apt-get update
    
    # install vim
    sudo apt-get install vim
    
    # more commands like eg
    # - install apache
    # - install mysql
    # - configure something
    # - etc
    
    # create a swap file
    sudo dd if=/dev/zero of=/swap bs=1M count=512
    sudo chmod 600 /swap
    sudo mkswap /swap
    echo "/swap  none  swap  sw  0  0" | sudo tee -a /etc/fstab
    
    # clear the box
    sudo apt-get clean
    # minimize packaged box size
    sudo dd if=/dev/zero of=/EMPTY bs=1M
    sudo rm -f /EMPTY
    # clear history
    cat /dev/null > ~/.bash_history && history -c && exit
    
When you are ready, logout and package the box

    vagrant package --output my-box.box
    
and destroy the machine

    vagrant destroy
    rm Vagrantfile
    
Then you can either import the box like this

    vagrant box add my/box my-box.box
    vagrant init my/box
    
Or you can take advantage of **box versioning**:

At first calculate the sha1 (or sha256, sha512, your call) of the box

    sha1sum my-box.box
    
And then create the file my-box.json with contents like

    {
        "name": "my/box",
        "description": "This box contains My Box",
        "versions": [
            {
                "version": "1.0.0",
                "providers": [{
                        "name": "virtualbox",
                        "url": "file:///path/to/my/box.box",
                        "checksum_type": "sha1",
                        "checksum": "-->CHECKSUM-HERE<--"
                }]
            }
        ]   
    }

after that, you can import the box with something like

    vagrant box add path/to/my-box.json
    vagrant init my/box
    
or by supplying it in the Vagrantfile:

    config.vm.box = "my/box"
    config.vm.box_url = "file://path/to/my-box.json"
    
By adjusting the above paths accordingly you can store the boxes along
with the json file (the box metadata) on a personal server.

Of course, the preffered way to create a vagrant environement is to use 
something like chef, puppet, ansible, etc, but this is simpler in the 
essence that you don't have to deal with any provisioning tools.

