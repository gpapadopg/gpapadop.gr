---
title: Redirect to https via .htaccess
tags:
    - Apache
---

    RewriteEngine on
    RewriteCond %{SERVER_PORT} !^443$
    RewriteRule ^.*$ https://%{SERVER_NAME}%{REQUEST_URI} [L,R=301]

The `[L]` flag causes `mod_rewrite` to stop processing the rule set.
The `[R]` flag issues an HTTP redirect to the browser (`301` stands for 
Permanent Redirect). This depends on `mod_rewrite` module.

For a complete reference head over to [apache docs](
https://httpd.apache.org/docs/2.4/rewrite/flags.html
).

