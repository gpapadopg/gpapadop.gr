---
title: Output amount via Currency library in OpenCart
tags:
    - OpenCart
---

For those in a hurry, here is an example on how to output an amount via OpenCart's `Currency` library from inside a **view** file:

    <?= $this->registry->get("currency")->format($amount) ?>

Eg a value of `200.50` is outputted as `250,50€` (assuming a Greek locale).

If you also need to do some math based on an existing number
(formatted as a string with currency code), you may do something like this:

    <?php
    // in order to convert "200,50€" to 200.50
    $convertToFloat = function($s) {
        // yeah I know... at least this gets the job done
        return (float)str_replace(",", ".", $s);
    };
    ?>
    <?= $this->registry->get("currency")->format($convertToFloat($price) * 1.24) ?>

