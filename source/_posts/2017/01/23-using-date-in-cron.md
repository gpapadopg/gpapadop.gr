---
title: Using date in cron
tags:
    - Cron
---

A simple way to have a command log output to a file grouping by date is to do
something like:

    # writes to a file my-command-YYYYMMDD.log
    my-command >> my-command-$(date +%Y%m%d).log
    
But in a cronjob you should escape the `%` character by prepending it with a 
backslash like this:

    * 0 * * * my-command >> my-command-$(date +\%Y\%m\%d).log
    
To capture both `stdout` and `stderr`:

    * 0 * * * my-command >> my-command-$(date +\%Y\%m\%d).log 2>&1

