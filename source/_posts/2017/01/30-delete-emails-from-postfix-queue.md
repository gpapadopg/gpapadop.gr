---
title: Delete emails from Postfix queue
tags:
    - Email
    - Postfix
    - Spam
---

On Postfix, to delete emails send by some domain (for example domain.tld), you can use:

    mailq | awk '$7 ~ /@domain.tld$/ { print $1 }' | tr -d '*!' | postsuper -d -

To delete from a specific sender:
 
    mailq | awk '$7 ~/^username@domain.tld$/ { print $1 }' | tr -d '*!' | postsuper -d -


Nb: To count all messages in queue use:

    mailq | grep -c '^[0-9A-Z]'
    # or
    mailq | grep -c '^\w'

Useful when your wordpress installation starts sending spam emails.

