---
title: Motherfucking Website
tags:
    - Web
---

I admit I love this: [motherfuckingwebsite.com](http://motherfuckingwebsite.com/)

It's dead simple: stop designing websites with 123456kB full of crap images and 
some javascript that halts your browser to the death.

I also have to admit I like this better: [bestmotherfucking.website](https://bestmotherfucking.website/)

Just look at the (reformatted) css:

```
body {
    margin: 1em auto;
    max-width: 40em;
    padding: 0 .62em;
    font: 1.2em/1.62em sans-serif;
}
h1, h2, h3 {
    line-height: 1.2em;
}
@media print {
    body {
        max-width: none;
    }
}
```

How awesome is that?

Side Note: Do you know you can use your own fonts and colors in your browser?
Random examples at [howtogeek.com](http://www.howtogeek.com/208552/how-to-change-the-default-fonts-in-your-web-browser/) and [support.mozilla.org](https://support.mozilla.org/el/kb/change-fonts-and-colors-websites-use)
