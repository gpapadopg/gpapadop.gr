---
title: Performance tips for Vagrant
tags:
    - Vagrant
---

- Set memory to 1/4 of the host.
- Try setting more than 1 CPU core but measure what best works for you.
- Also enable IOAPIC if using more than one CPU core.
- Enable KVM virtualization if on a GNU/Linux host.

An example with these settings applied:

    config.vm.provider "virtualbox" do |vb|
        vb.memory = "1024"
        vb.cpus = 2
        vb.customize ["modifyvm", :id, "--ioapic", "on", "--paravirtprovider", "kvm"]
    end


