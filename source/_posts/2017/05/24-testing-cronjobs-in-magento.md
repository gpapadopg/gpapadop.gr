---
title: Testing cron jobs in Magento
tags:
    - Magento
---

At first, find out how the cronjob is called from the module's `config.xml`
by looking at the `/config/crontab/jobs/*/run/model` XPath, eg:
 
    <config>
      ...
      <crontab> 
	    <jobs> 
	      <my_cronjob>
		    <run>
		      <model>my_cronjob/observer::my_method</model>
			</run>
			...

On this example, you need to initialize the `my_cronjob/observer` model 
and call the method `my_method` on it. In order to do this create a file to *run 
from command line* with these contents:

    <?php
    // initialize magento application
    require_once '/path/to/app/Mage.php';    
    Mage::app();

    // initialize model and run the method
    $myCronJob = Mage::getModel('my_cronjob/observer');
    $myCronJob->my_method();

