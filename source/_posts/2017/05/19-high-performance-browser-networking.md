---
title: High Performance Browser Networking
tags:
    - Optimization
    - Performance
---

A book from [Ilya Grigorik](https://www.igvita.com/), web performance engineer
at Google:

> Performance is a feature. This book provides a hands-on overview of what 
> every web developer needs to know about the various types of networks
> (WiFi, 3G/4G), transport protocols (UDP, TCP, and TLS), application
>  protocols (HTTP/1.1, HTTP/2), and APIs available in the browser
> (XHR, WebSocket, WebRTC, and more) to deliver the best - fast, reliable
> and resilient - user experience. 

Available to read online at [hpbn.co](https://hpbn.co/)
