---
title: Writing a Git commit message
tags:
    - Git
---

A good commit message should answer three questions, in order to establish the
context the commit is applied:

- Why is it necessary?
- How does it address the issue?
- What effects does it have?

Of course, there's no ideal commit message, but some general rules:

**Use imperative statements in the subject line**.
For example, write "Fix broken link" instead of "Fixed broken link".
Write them in order to complete the following sentence: If applied, 
this commit will *Fix broken link*

**Use 50 characters or less in the subject line.**
Keeping subject lines under the 50 characters limit ensures that they are
readable and concise. If it's difficult to summarize, it may be because it
includes several logical changes or bug fixes. Also avoid lazy or general
messages.

**Do not end the subject line with a period.** 
It's a title and titles don't end with a period.

**Begin the subject line with capitalized text.**
Simple as that.

**Separate subject from message body with a blank line.**
Mostly useful when browsing the log.

**Wrap the message body within a 72 characters limit.**
Because conventions. And `git log`.

**Explain what and why and not how.**
Show the intent and the context. The how is easy for anyone to see by
simply doing a `git diff`.

**Mention associated issues at the end of the body.**
This is usually the place to be.

## (Some) References:

- [chris.beams.io/posts/git-commit](https://chris.beams.io/posts/git-commit/)
- [who-t.blogspot.gr/2009/12/on-commit-messages.html](http://who-t.blogspot.gr/2009/12/on-commit-messages.html)
- [github.com/erlang/otp/wiki/writing-good-commit-messages](https://github.com/erlang/otp/wiki/writing-good-commit-messages)
- [github.com/torvalds/subsurface-for-dirk/blob/master/README#L92-L120](https://github.com/torvalds/subsurface-for-dirk/blob/master/README#L92-L120)
- [git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project#_commit_guidelines](https://www.git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project#_commit_guidelines)
