---
title: Replacing Disqus with Github Comments
tags:
    - Disqus
---

In this post at [http://donw.io/post/github-comments/](http://donw.io/post/github-comments),
Don Williamson describes his findings of replacing Disqus with GitHub comments.

Some interesting facts:

> * Load-time goes from 6 seconds to 2 seconds.
> * There are 105 network requests vs. 16.
> * There are a lot of non-relevant requests going through to networks that will be tracking your movements.

The author continues by listing some of these tracking networks when using the 
disqus platform:

> * disqus.com
> * google-analytics.com
> * connect.facebook.net
> * accounts.google.com
> * pippio.com
> * bluekai.com
> * crwdcntrl.net
> * exelator.com
> * doubleclick.net
> * tag.apxlv.net
> * adnxs.com
> * adsymptotic.com
> * rlcdn.com
> * adbrn.com
> * nexac.com
> * tapad.com
> * liadm.com
> * sohern.com
> * demdex.net
> * bidswitch.net
> * agkn.com
> * mathtag.com

And this is the important point:

> Needless to say, it’s a pretty disgusting insight into how certain free 
> products turn you into the product.

Read the whole article at [http://donw.io/post/github-comments/](http://donw.io/post/github-comments),
