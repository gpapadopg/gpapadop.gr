---
title: Improving javascript load/parse times
tags:
    - Javascript
    - Optimization
---

Loading a webpage is much more than just downloading the page content. For the 
javascript part, the browser has to download, parse, interpret and then run 
the javascript... scripts.

In [this](https://medium.com/@addyosmani/javascript-start-up-performance-69200f43b201)
post, Addy Osmani, staff engineer (is this a thing?) at Google,
shows some notes on how javascript may slow down a web page and how you
may speed things up.

Most notable quotes:

> Historically, we just haven’t spent a lot of time optimizing for the 
> JavaScript Parse/Compile step. We almost expect scripts to be immediately 
> parsed and executed as soon as the parser hits a `<script>` tag. But this 
> isn’t quite the case...

> Parsing, Compiling and Executing scripts are things a JavaScript engine 
> spends significant time in during start-up. This matters as if it takes a 
> while, it can delay how soon users can interact with our site...

> As we move to an increasingly mobile world, it’s important that we understand 
> the time spent in Parse/Compile can **often be 2–5x as long on phones as on 
> desktop**...

> Script size is important, but it isn’t everything. Parse and Compile times 
> don't necessarily increase linearly when the script size increases...

In order to improve parse times, the author suggest on the following:

> - Ship less JavaScript
> - Use code-splitting to only ship the code a user needs
> - Script streaming
> - Measure the parse cost of dependencies

So, next time, beware using two tons of javascript libraries to manipulate
some DOM elements.
