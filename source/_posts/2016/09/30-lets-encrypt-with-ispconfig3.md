---
title: Let's Encrypt with ISPConfig3
tags:
    - Let's Encrypt
    - ISPConfig
---

Note: This applies on Ubuntu Server 14.04 with Apache web server.

First setup Let's Encrypt client, [Certbot](https://certbot.eff.org/):

	# download client
	wget https://dl.eff.org/certbot-auto
	
	# make it executable
	chmod a+x certbot-auto
	
	# make it globally available
	mv -iv certbot-auto /usr/bin

	# first time setup
	certbot-auto

Then, to request certificates for the domain EXAMPLE.DOMAIN, run:

	certbot-auto certonly \
		--webroot \
		--webroot-path /var/www/EXAMPLE.DOMAIN/web/ \
		--domains EXAMPLE.DOMAIN \
		--email EXAMPLE@EMAIL

Certificate and chain are saved in directory `/etc/letsencrypt/live/EXAMPLE.DOMAIN/`.

After that, configure SSL in ISPConfig by enabling the SSL checkbox on EXAMPLE.DOMAIN 
and setting the keys on the SSL tab:

- Copy the contents of `/etc/letsencrypt/live/EXAMPLE.DOMAIN/privkey.pem` into the **SSL Key**.
- `/etc/letsencrypt/live/EXAMPLE.DOMAIN/cert.pem` to **SSL Certificate**.
- `/etc/letsencrypt/live/EXAMPLE.DOMAIN/chain.pem` to **SSL Bundle**.

Also, don't forget to select "Save Certificate" on SSL Action.
	
Because Let’s Encrypt certificates are valid for a short period of time, they should be automatically renewed.
To do this you have to setup a cronjob to run once or twice per day:

	# the --post-hook is optional, and is used in this case to restart the apache webserver
	echo '42 3 * * *  root  certbot-auto renew --post-hook "service apache2 restart"' > /etc/cron.d/certbot

Finally, to not break things with ISPConfig, link the certificates with Let's Encrypt:

	ln -sf /etc/letsencrypt/live/EXAMPLE.DOMAIN/cert.pem 	EXAMPLE.DOMAIN.crt
	ln -sf /etc/letsencrypt/live/EXAMPLE.DOMAIN/privkey.pem EXAMPLE.DOMAIN.key
	ln -sf /etc/letsencrypt/live/EXAMPLE.DOMAIN/chain.pem 	EXAMPLE.DOMAIN.bundle

Note: To redirect HTTP to HTTPS, you may use the following `.htaccess` snippet:

	RewriteEngine on
	RewriteCond %{SERVER_PORT} !^443$
	RewriteRule ^.*$ https://%{SERVER_NAME}%{REQUEST_URI} [L,R=301]
