---
title: Do you know SSRF
tags:
    - Web application securiry
---

An interesting article on Server Side Request Forgery (SSRF):

> In this attack, specific payloads for different ports are crafted by the attacker and sent to the server. 
> By analyzing the errors or the time-delays in different responses for different ports, 
> the attacker can figure out the status of the ports open on the server. And while exploiting SSRF, 
> the attacker’s machine is not directly interacting with the target server, the vulnerable server is 
> doing all the dirty work for the attacker.

See more at [http://niiconsulting.com/checkmate/2015/04/server-side-request-forgery-ssrf](http://niiconsulting.com/checkmate/2015/04/server-side-request-forgery-ssrf).

