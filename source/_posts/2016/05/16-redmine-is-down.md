---
title: Redmine (1.3) is down
tags:
    - Redmine
---

Recently our redmine issue tracker stopped working responding with internal server error 500.
The log file in `/var/log/redmine/default/production.log` was showing errors like this:

    Status: 500 Internal Server Error
    private method `split' called for nil:NilClass

After a google search, the solution found on the [redmine forum](https://www.redmine.org/boards/2/topics/49303):
For some reason, *the issue seems to be caused by a passenger security update from the debian-lts team* 
on the `libapache2-mod-passenger` package, so I have to rollback:

    # downgrade package
    aptitude install libapache2-mod-passenger=2.2.11debian-2
    
    # and pin it via
    aptitude hold libapache2-mod-passenger
    # or
    apt-mark hold libapache2-mod-passenger

