#!/bin/bash

vendor/bin/sculpin generate --env=prod
if [ $? -ne 0 ]; then echo "Could not generate the site"; exit 1; fi

rsync --delete-after -avze ssh output_prod/ root@gpapadop.gr:/var/www/gpapadop.gr/public/
if [ $? -ne 0 ]; then echo "Could not publish the site"; exit 1; fi

